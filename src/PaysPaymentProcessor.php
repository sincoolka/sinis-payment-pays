<?php
namespace Su\Sinis\Payments\Pays;

use Nette;


class PaysPaymentProcessor extends Nette\SmartObject
{
    private $merchantHMACKey;

    public function __construct($merchantHMACKey)
    {
        $this->merchantHMACKey = $merchantHMACKey;
    }

    /**
     * Verifies the signature of payment order data using HMAC with MD5 hashing function.
     *
     * @param PaysResult $payment The object with payment order results
     * @return boolean TRUE if signature is valid, else FALSE.
     */
    public function verifySignature(PaysResult $payment)
    {
        if(!isset($payment->hash)) {
            return FALSE;
        }

        $dataToHash = sprintf("%s%s%s%s%d%d",
            $payment->paymentOrderId,
            $payment->merchantOrderId,
            $payment->paymentOrderStatusId,
            $payment->currency,
            $payment->sumInCents,
            $payment->currencyBaseUnits
        );
        $hash = hash_hmac("md5", $dataToHash, $this->merchantHMACKey);

        if (strtolower($payment->hash) === $hash) {
            return TRUE;
        }

        return FALSE;
    }
}