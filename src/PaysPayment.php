<?php
namespace Su\Sinis\Payments\Pays;

use Nette;

/**
 * Representation of payment request
 *
 * @author Pavel Valach
 *
 * @property       string $currency
 * @property       float $sum
 * @property-read  int $sumInCents
 * @property       string $merchantOrderId
 * @property       string $email
 */
class PaysPayment extends Nette\SmartObject
{    
    /** @var string */
    public $currency = "CZK";
    
    /** @var float */
    public $sum;

    /** @var string */
    public $customerEmail;
    
    /** @var string */
    public $merchantOrderId;
}