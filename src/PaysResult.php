<?php
namespace Su\Sinis\Payments\Pays;

use Nette;

/**
 * Representation of returned result of payment
 *
 * @author Pavel Valach
 *
 * @property-read  float $sum
 * @property       int $sumInCents
 * @property       string $paymentOrderId
 * @property       string $merchantOrderId
 * @property       string $currency
 * @property       int $currencyBaseUnits
 * @property       int $paymentOrderStatusId
 * @property       string $paymentOrderStatusDescription
 * @property       string $hash
 */
class PaysResult extends Nette\SmartObject
{
    /** @var float */
    public $sumInCents;

    /** @var string */
    public $paymentOrderId;
    
    /** @var string */
    public $merchantOrderId;
    
    /** @var string */
    public $currency = "CZK";

    /** @var int */
    public $currencyBaseUnits;

    /** @var int */
    public $paymentOrderStatusId;

    /** @var string */
    public $paymentOrderStatusDescription;

    /** @var string */
    public $hash;

}